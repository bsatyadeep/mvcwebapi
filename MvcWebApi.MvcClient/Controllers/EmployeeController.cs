﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using MvcWebApi.MvcClient.Models;
using Newtonsoft.Json;

namespace MvcWebApi.MvcClient.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly HttpClient client;
        private const string url = "http://localhost:52002/api/Employee";
        public EmployeeController()
        {
            client = new HttpClient {BaseAddress = new Uri(url)};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        // GET: Employee
        public async Task<ActionResult> Index()
        {
            var httpResponseMessage = await client.GetAsync(url);
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                var employees = JsonConvert.DeserializeObject<List<EmployeeInfo>>(result);
                return View(employees);
            }
            return View("Error");
        }

        [HttpPost]
        public async Task<ActionResult> Index(string department)
        {
            var httpResponseMessage = await client.GetAsync(url);
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var result = httpResponseMessage.Content.ReadAsStringAsync().Result;                
                var employees = JsonConvert.DeserializeObject<List<EmployeeInfo>>(result);
                var departments = employees.Where(e => e.DeptName.Contains(department)).Select(e => e.DeptName).Distinct().ToList();
                return Json(departments,JsonRequestBehavior.AllowGet);
            }
            return View("Error");
        }
        public ActionResult Create()
        {
            return View(new EmployeeInfo());
        }

        //The Post method
        [HttpPost]
        public async Task<ActionResult> Create(EmployeeInfo Emp)
        {
            if (!ModelState.IsValid)
                return View(Emp);
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync(url, Emp);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Error");
        }
        public async Task<ActionResult> Edit(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var Employee = JsonConvert.DeserializeObject<EmployeeInfo>(responseData);

                return View(Employee);
            }
            return View("Error");
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, EmployeeInfo Emp)
        {

            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(url + "/" + id, Emp);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Error");
        }

        public async Task<ActionResult> Details(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var Employee = JsonConvert.DeserializeObject<EmployeeInfo>(responseData);

                return PartialView("_Details",Employee);
            }
            return View("Error");
        }
        public async Task<ActionResult> Delete(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var Employee = JsonConvert.DeserializeObject<EmployeeInfo>(responseData);

                return View(Employee);
            }
            return View("Error");
        }

        //The DELETE method
        [HttpPost]
        public async Task<ActionResult> Delete(int id, EmployeeInfo Emp)
        {

            HttpResponseMessage responseMessage = await client.DeleteAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Error");
        }
    }
}