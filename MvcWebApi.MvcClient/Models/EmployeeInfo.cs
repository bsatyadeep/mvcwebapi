﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MvcWebApi.MvcClient.Models
{
    public class EmployeeInfo
    {
        [Key]
        public int EmpNo { get; set; }

        [DisplayName("Employee Name")]
        [Required(ErrorMessage = "Employee name most be filled.")]
        [StringLength(250,MinimumLength = 5,ErrorMessage = "Employee name most be between {1} charecters and {2} charecters.")]
        public string EmpName { get; set; }
        public decimal Salary { get; set; }

        [DisplayName("Department Name")]
        [Required(ErrorMessage = "Department name most be filled.")]
        [StringLength(250, MinimumLength = 5, ErrorMessage = "department most be between {1} charecters and {2} charecters.")]
        public string DeptName { get; set; }

        [DisplayName("Designation")]
        [Required(ErrorMessage = "Designation name most be filled.")]
        [StringLength(250, MinimumLength = 5, ErrorMessage = "Designation most be between {1} charecters and {2} charecters.")]
        public string Designation { get; set; }
    }
}