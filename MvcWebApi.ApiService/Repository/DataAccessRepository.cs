﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using MvcWebApi.ApiService.Models;

namespace MvcWebApi.ApiService.Repository
{
    public class DataAccessRepository:IDataAccessRepository<EmployeeInfo,int>
    {
        [Dependency]
        public ApplicationEntities Context { get; set; }

        public IEnumerable<EmployeeInfo> Get()
        {
            return Context.EmployeeInfoes.ToList();
        }

        public EmployeeInfo Get(int id)
        {
            return Context.EmployeeInfoes.Find(id);
        }

        public void Post(EmployeeInfo entity)
        {
            Context.EmployeeInfoes.Add(entity);
            Context.SaveChanges();
        }

        public void Put(int id, EmployeeInfo entity)
        {
            var emp = Context.EmployeeInfoes.Find(entity.EmpNo);
            if (emp != null)
            {
                emp.EmpName = entity.EmpName;
                emp.Salary = entity.Salary;
                emp.DeptName = entity.DeptName;
                emp.Designation = entity.Designation;
                Context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            var emp = Context.EmployeeInfoes.Find(id);
            if (emp != null)
            {
                Context.EmployeeInfoes.Remove(emp);
                Context.SaveChanges();
            }
        }
    }
}