using Microsoft.Practices.Unity;
using System.Web.Http;
using MvcWebApi.ApiService.Models;
using MvcWebApi.ApiService.Repository;
using Unity.WebApi;

namespace MvcWebApi.ApiService
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<IDataAccessRepository<EmployeeInfo, int>,DataAccessRepository>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}